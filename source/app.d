module app;

import std.stdio;

import core.managed.inject;
import core.managed.managed;

import core.managed.registry;

import core.managed.context;

@("attr1")
@("attr2")
class Asdf
{
    mixin Managed;

    @Inject("hello")
    this(@Qualifier("paramAnnotation") int a, float b, double c)
    {

    }

    @Inject("hello")
    string fieldHello;

    @Inject("bean")
    int test(int x)
    {
        return 5;
    }

    override
    {
        string toString()
        {
            return super.toString() ~ "[fieldHello="~fieldHello~"]";
        }
    }
}

class Yxcv : Asdf
{
    mixin Managed;

    this()
    {
        super(1, 2, 3);
    }
}

class Qwerty
{
    mixin Managed;
}

class DependsOnQwerty
{
    mixin Managed;

    Qwerty qwerty;

    this(Qwerty qwerty) {
        this.qwerty = qwerty;
    }
}

string className(T)()
{
    return T.classinfo.name;
}

void main()
{
    import std.variant;

    writeln("Edit source/app.d to start your project.");

    ApplicationContext appContext = ApplicationContext("app");

    writeln("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    Qwerty instance = appContext.getInstance!Qwerty();

    Asdf asdf = appContext.getInstance!Yxcv();

    Asdf yxcv = appContext.getInstance!Yxcv();

    DependsOnQwerty dep = appContext.getInstance!DependsOnQwerty();

    Variant yxcvVar = Variant(yxcv);

    Asdf asdfFromVar = yxcvVar.get!Asdf();

    writeln(dep.qwerty);
}

