module core.managed.managed;

import core.managed.info : VariableInfo, FunctionInfo, FunctionParamInfo;

import std.stdio;

alias attributes(T...) = __traits( getAttributes, T );

alias member(T...) = __traits(getMember, T);

alias memberAttributes(T... ) = attributes!(member!(T));

alias derivedMembers(T...) = __traits( derivedMembers, T);

template qualifier( slicedAttr... )
{
    import core.managed.inject : Qualifier;

    string make(size_t n)()
    {
        static if (n < slicedAttr.length)
        {
            enum attr = slicedAttr[n];
            static if( is( typeof(attr) == Qualifier ) )
            {
                Qualifier qualifier = attr;
                return qualifier.name;
            }
            else
            {
                return make!(n+1)();
            }
        }
        else
            return "";
    }

    static if( slicedAttr.length == 0 )
    {
        enum qualifier = "";
    }
    else
    {
        enum qualifier = make!0();
    }
}

template functionParameters( T... )
{
    import core.managed.inject : Inject;

    import help.typeinfo;

    alias func = member!(T);
    static if( is( typeof(func) funcParams == __parameters) )
    {
        template fnParams()
        {
            template functionParameter(size_t varIndex)
            {
                alias sliced = funcParams[varIndex .. varIndex + 1];
                alias slicedAttr = __traits( getAttributes, sliced );

                enum typeName = fullTypeName!(sliced[0]);

                enum name = __traits(identifier, sliced);

                enum qualifierStr = qualifier!( slicedAttr );

                enum functionParameter = FunctionParamInfo(typeName, name, qualifierStr);
            }

            FunctionParamInfo[] make(size_t n)()
            {
                static if (n < funcParams.length)
                    return functionParameter!n ~ make!(n+1)();
                else
                {
                    FunctionParamInfo[0] arr = [];
                    return [];
                }
            }

            enum fnParams = make!0();
        }

        enum functionParameters = fnParams!();
    }
    else
    {
        static assert(0, "arguments do not name a function");
    }
}

template GenReturnCall(string prefix, string func, funcParams...)
{
    string makeSingle(size_t n, string type)() {
        return "args[" ~ n.stringof ~ "].get!" ~ funcParams[n].stringof;
    }

    string make(size_t n)()
    {
        static if ( (n + 1) == funcParams.length)
            return makeSingle!(n, funcParams[n].stringof);
        else static if (n < funcParams.length)
            return makeSingle!(n, funcParams[n].stringof) ~ ", " ~ make!(n+1)();
        else
            return "";
    }

    enum argStr = make!0();

    const char[] GenReturnCall = "return Variant(" ~prefix ~ func ~ "(" ~ argStr ~ ") );";
}

template GenImport(T)
{
    const char[] GenImport = "import " ~ __traits(parent, T).stringof ~ ";";// ~ " : " ~ T.stringof ~ ";";
}

template allTypes(T) {
    enum base = T.classinfo.base;
    pragma( msg, base);

    enum allTypes = [fullTypeName!base];
}

mixin template handleConstructor(T, string mem) {
    import std.traits : Parameters, ReturnType;
    import std.typecons : Nullable;
    import std.variant;

    alias func = member!(T, mem);
    alias retType = ReturnType!( func );
    enum retTypeClassInfo = retType.classinfo;
    enum retTypeStr = retType.stringof;
    enum funcAttr = attributes!( func );

    pragma( msg, retType );
    //pragma( msg, allTypes!retType );

    alias funcParams = Parameters!func;

    enum funcParamInfo = functionParameters!( T, mem );

    Variant function(Variant[]) instantiator = (Variant[] args) {
        mixin( GenReturnCall!("new ", T.stringof, funcParams) );
    };

    Nullable!FunctionInfo handleConstructor =
        Nullable!FunctionInfo( FunctionInfo(mem, retTypeStr, mem, instantiator, funcParamInfo) );
}

mixin template handleFunction(T, string mem)
{
    import std.traits : Parameters, ReturnType;
    import std.typecons : Nullable;

    static if( mem == "__ctor" )
    {
        mixin handleConstructor!(T, mem);
        Nullable!FunctionInfo handleFunction = handleConstructor;
    }
    else
    {
        enum handleFunction = Nullable!FunctionInfo.init;
    }

}

VariableInfo* handleVariable(T, string mem)()
{
    import std.variant;
    import core.managed.inject : Inject;

    alias var = member!(T, mem);
    alias varType = typeof( var );
    auto varAttr = attributes!(var);

    writeln("variable " ~ mem);

    if(varAttr.length == 0) {
        return null;
    }

    static foreach( i, attr ; varAttr)
    {
        static if( is( typeof(attr) == Inject ) )
        {
            Inject inject = attr;

            VariableInfo* info = new VariableInfo;
            info.variableName = mem;
            info.key = inject.name;
            info.typeName = varType.stringof;
            info.setter = function void(Object inst, Variant val) {
                __traits(getMember, cast(T)inst, mem) = val.get!( varType );
            };

            writeln("variable " ~ mem ~ " end" );

            return info;
        }
        else static if ( i + 1 == varAttr.length )
        {
            writeln("variable " ~ mem ~ " end" );
            return null;
        }

    }

}

mixin template Managed(string name = null)
{
    import help.ensureconstructor : ensureconstructor;
    import std.typecons : Nullable;

    mixin ensureconstructor;

    shared static this()
    {
        import std.traits : isFunction, ReturnType;
        import core.managed.info : ManagedClassInfo, VariableInfo, FunctionInfo;
        import core.managed.registry : managedClasses;

        alias type = typeof(this);

        TypeInfo_Class typeInfo = type.classinfo;

        ManagedClassInfo info;
        info.typeName = typeInfo.name;
        info.name = name ? name : typeInfo.name;

        writeln("Component begin: " ~ info.name);

        writeln( "base " ~ typeInfo.base.name );

        static foreach( f; derivedMembers!( type ) )
        {{

            static if( isFunction!( member!( type, f) ) )
            {
                mixin handleFunction!(type, f);
                Nullable!FunctionInfo funcInfo = handleFunction;
                if(!funcInfo.isNull)
                {
                    info.functions[f] = funcInfo;
                }
            }
            else
            {
                VariableInfo* varInfo = handleVariable!(type, f)();
                if(varInfo)
                {
                    info.variables[f] = *varInfo;
                }
            }

        }}

        writeln("Component end");

        managedClasses[info.typeName] = info;
    }

}
