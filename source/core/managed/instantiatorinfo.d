module core.managed.instantiatorinfo;

import std.variant;

import core.managed.info;

struct InstantiatorInfo {
    string className;
    string name;
    FunctionParamInfo[] params;
    Variant function(Variant[]) instantiator;

    this(string className, string name, FunctionParamInfo[] params, Variant function(Variant[]) instantiator) {
        this.className = className;
        this.name = name;
        this.params = params;
        this.instantiator = instantiator;
    }

}

struct PostConstructInfo {
    private string className;

    this(string className) {
        this.className = className;
    }
}
