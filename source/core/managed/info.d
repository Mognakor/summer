module core.managed.info;

import std.variant;

struct ManagedClassInfo
{
    string typeName;

    string name;

    VariableInfo[string] variables;

    FunctionInfo[string] functions;


    string toString()
    {
        string str = "ManagedClassInfo[typeName="~typeName~", name="~name ~",\n variables={";

        foreach(string key; variables.keys) {
            VariableInfo varInfo = variables[key];
            str = str ~ key ~ "=" ~ varInfo.toString() ~ " ";
        }

        str = str ~ "},\n functions={";

        foreach(string key; functions.keys) {
            FunctionInfo funcInfo = functions[key];
            str = str ~ key ~ "=" ~ funcInfo.toString() ~ "\n";
        }

        return str ~ "} ]";
    }

}

struct VariableInfo
{
    string variableName;
    string typeName;

    string key;

    void function(Object, Variant) setter;

    string toString() {
        return "VariableInfo[variableName=" ~ variableName ~ ", typeName=" ~ typeName ~ "key=" ~ key ~ ", setter=" ~ setter.stringof ~ "]";
    }
}

struct FunctionInfo
{
    this(string functionName, string returnTypeName, string key,
            Variant function(Variant[]) instantiator, FunctionParamInfo[] params)
    {
        this.functionName = functionName;
        this.returnTypeName = returnTypeName;
        this.key = key;
        this.instantiator = instantiator;
        this.params = params;
    }

    string functionName;
    string returnTypeName;

    string key;

    Variant function(Variant[]) instantiator;

    FunctionParamInfo[] params;

    string toString()
    {
        string str = "FunctionInfo[functionName="~functionName~", returnTypeName="~returnTypeName ~ ", key=" ~ key ~ ", params={";

        foreach(FunctionParamInfo paramInfo; params) {
            str = str ~ paramInfo.toString() ~ ",\n ";
        }

        str = str ~ "} ] ";

        return str;
    }
}

struct FunctionParamInfo
{
    this(string typeName, string name, string qualifier)
    {
        this.typeName = typeName;
        this.name = name;
        this.qualifier = qualifier;
    }

    string typeName;
    string name;
    string qualifier;

    string toString() {
        return "FunctionParamInfo[typeName=" ~ typeName ~ ", name=" ~ name ~ ", qualifier=" ~ qualifier ~ "]";
    }
}

