module core.managed.context;

import std.stdio;
import std.variant;

import core.managed.info;
import core.managed.registry;
import core.managed.instantiatorinfo;

struct ApplicationContext {
    private const InstantiatorInfo[string] contextClasses;

    this(string contextPath) {
        InstantiatorInfo[string] contextClasses;
        foreach(string className; managedClasses.keys)
        {
            writeln("Key: " ~ className);

            ManagedClassInfo managedClass = managedClasses[className];

            foreach(string funcKey; managedClass.functions.keys) {
                FunctionInfo funcInfo = managedClass.functions[funcKey];

                if(funcInfo.key == "__ctor" ) {
                    contextClasses[className] = InstantiatorInfo(className, funcInfo.key, funcInfo.params, funcInfo.instantiator);
                }

            }

            writeln(managedClass);
        }

        this.contextClasses = contextClasses;
    }

    public T getInstance(T)() {
        string className = T.classinfo.name;
        writeln(className);

        bool[string] visitedClasses;

        Variant var = getInstance(className, visitedClasses);

        return var.get!T();
    }

    private Variant getInstance(string className, bool[string] visitedClasses) {
        const InstantiatorInfo* classInfo = (className in this.contextClasses);
        if(classInfo == null) {
            throw new Exception("Could not find a managed instance of type " ~ className);
        }

        const bool visited = visitedClasses.get(className, false);
        if(visited) {
            string treeMembers = "[";
            foreach(string key; visitedClasses.keys) {
                treeMembers = treeMembers ~ key ~ ", ";
            }
            treeMembers = treeMembers ~ "]";

            throw new Exception("Class " ~ className ~ " already is present in dependency tree. Tree: " ~ treeMembers);
        }

        visitedClasses[className] = true;

        const FunctionParamInfo[] paramInfos = classInfo.params;

        Variant[] arguments;

        foreach(FunctionParamInfo info; paramInfos) {
            arguments = arguments ~ getInstance(info.typeName, visitedClasses);
        }

        visitedClasses.remove(className);

        return classInfo.instantiator(arguments);
    }

}

