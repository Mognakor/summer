module help.typeinfo;

template fullTypeName(T){
    static if( is( typeof( T.init )  : Object) ) {
        // workaround since classinfo.name does not work at compile-time
        enum typeIdStr = typeof( T.init ).classinfo.stringof;
        enum fullTypeName = typeIdStr[8 .. $-1];
    } else {
        enum fullTypeName = T.stringof;
    }
}
