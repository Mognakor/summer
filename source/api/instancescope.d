module api.instancescope;

import std.variant;

interface InstanceScope {
    Variant get(string name, Variant function() instanceProvider);
}

class PrototypeScope : InstanceScope {
    Variant get(string name, Variant function() instanceProvider) {
        return instanceProvider();
    }
}

class SingletonScope : InstanceScope {
    Variant[string] singletons;

    Variant get(string name, Variant function() instanceProvider) {
        return singletons.get(name, instanceProvider());
    }
}
